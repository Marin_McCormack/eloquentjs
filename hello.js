// This is an example program to explore JavaScript!
// Print some intro text
console.log("Hello world!");
console.log("This is an example program to show some JavaScript stuff!");

// Define our input/output interface
// This imports nodejs's built-in readline module and creates an
// interface with input from stdin and output to stdout.
const readline = require('readline').createInterface({
	input: process.stdin,
	output: process.stdout
})

readline.question("What is your name? ", name => {
	console.log("Do I like " + name + "?");
	var tempNum = Math.random();
	if (tempNum < 0.5) {
		console.log("Yes!");
	} else {
		console.log("No!");
	}
	process.exit(0);
})


