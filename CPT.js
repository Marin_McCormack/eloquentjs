const readline = require('readline').createInterface({
	input: process.stdin,	output: process.stdout
})

function randomNumber(max) {
	return Math.floor(Math.random() *  max);
}

const art = ["make a paper snowflake", "glitter glue", "painting", "color a coloring book", "watch a youtube video on how to draw something"]
const out = ["go hiking", "go swimming", "play a sport", "walk the dog", "go biking"]
const tech = ["play a video game", "read an online book", "listen to music", "go on social media", "text your friends"]
readline.question("What type of activity would you like to do?(Arts, Outdoors, Technology) ", activityType => {

switch (activityType.toLowerCase()){
	case "arts":
		console.log("You should " + art[randomNumber(art.length)] + ".");
		break;
	case "outdoors":
		console.log("You should " + out[randomNumber(out.length)] + ".");
		break;
	case "technology":
		console.log("You should " + tech[randomNumber(tech.length)] + ".");
		break;
}

process.exit();
})
