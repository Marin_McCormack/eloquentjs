# Test Review Three

# Question 1
## Name: Searching a List for a Value
## Catagorey: Algorithmic Efficiency
Consider the following procedures (A, B, and C) that are all designed to
search the list aList for the value 23 and return the index in the list
where 23 is found. If the number 23 is not in the list, -1 should be 
returned.
A

PROCEDURE A(aList)
{
index ← 1
FOR EACH item in aList
{
      IF(item = 23) 
      {
         RETURN(index)
         index ← index + 1
       }
}
RETURN("-1")
}
## Answer: 
There is actually an error in this problem. All of the answers say that A
works correctly but it doesn't because there is a return in the IF statement
that would exit the if statemnt and the program would run in an infinate
loop. Also the index ← index + 1 is in the wrong spot it really should be in
the FOR statement/ after the closed parenthase of the if statement. If we
were to make this correct it would look like this: 

PROCEDURE A(aList)
 {
 index ← 1
 FOR EACH item in aList
 {
       IF(item = 23)
       {
          RETURN(index)
        }
    index ← index + 1
 }
 RETURN("-1")
 }

# Question 2
## Name: Game Show
## Catagory: Developing Procedures
Assume there is a procedure called rollDie(), which correctly returns a random integer between 1 and 6
inclusively and all values are equally likely.
A game is played where a contestant rolls a pair of dice. If both dice result in 1
, then the contestant wins 25 dollars. If one of the dice results in 1, then the contestant rolls the pair of dice again. In this case, the player wins the sum of the dice in dollars, for example, 4 and 3 result in 7
dollars. In all other cases, the contestant wins no money.
A procedure called playGame is shown below:

PROCEDURE playGame()
{
       first ← rollDie()
       second ← rollDie()

       IF(first = 1   AND    second = 1)
       {
                RETURN("25")
       }

       IF(first = 1    OR    second = 1)
       {
                RETURN(first + second)
       }

       RETURN("0")

}

However, the procedure has an error. Which of the following is the best description of the error?
## Answer:
Two more calls to rollDie() need to be made in the second IF statement.
## What I chose: 
The order of the IF statements should be swapped. 
## Why its wrong:
 If the IF statements are swapped, when the procedure is supposed to return
25, it will always return the sum of the dice rolled. If I read the 
directions more carefully then I would have known the answer because it
literally sayd the answer in the directions. 

# Question 3
## Name: Largest Integer
## Catagory: Binary Numbers
A numbering system is developed creating binary numbers with 5 bits. In base
-10, what are the lowest and highest values that can be constructed in this
numbering system?
## Answer: 
Lowest = 0
Highest = 31
## What I chose: 
Lowest = 0
Highest = 15
## Why it's wrong:
15 would be the highest decimal value if we were using 4 bits. The largest
decimal value with n bits is 2n−1. The decimal number 15 can be represented 
as 1111 in binary.
 
