# Google Trends Chocolate Searches
- The United States experiences spikes anually around the holidays Christmas, Valentine's Day, and Easter. Christmas has the most searches, then Valentines Day then Easter.  
- At 5:17 this morning Virginia experienced a spike in chocolate searches 
- Over the past 5 years Vermont had the most searches for chocolate while Mississippi had the least
- A related search is hot chocolate bombs which remains at 0 for almost the entire span of 5 years exept for late 2019 where it experiences a small jump. In late 2020 it experiences a lage spike and lastly in 2021 it experiences a noticably smaller spike. The spikes are all in the month of December. 
- West Virginia has the overall most searches over the past 5 years for hot chocolate bombs. 

## Misc. Searches 
### "git repo"
- Searches increase steadly up to 2020 and then start slowly droping. However only looking at recent data within the past 5 years reveals no discernable pattern. There seems to be regular drops in searches around december. 
### "winter olympics"
- Since 2004 there have been little to no activity except for noticably every 4 years in February where searches spike drastically. There are also smaller jumps in searches in August around the time that the summer olympics end.   
