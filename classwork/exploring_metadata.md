# Metadata Notes
## Most Spoken Languages 
- The data contains rank, the language name, speakers in millions, the % of world population, language family and branch.
- The top spoken laguages are Mandarin, Spanish, and English.
- The % of the world population that speaks Mandarine Chinese is more than twice that of the second most spoken language.
- Most languages come from the Indo-European language family
- The least spoken listed language is Sylheti
## NFL Teams 
- Conference (string): whether the team is in the AFC or NFC conference
- Division (string): what division the team is in, such as North or East
- Team (string): name of the team
- City (string): city and state the team is located in
- Stadium (string): name of the stadium the team plays home games in
- Capacity (numerical): how many seats the arena holds for attendees
- Head coach (string): name of the current head coach of the team
- Image (link): a link to an image of the team's logo
## Grammy Winners
- Year (numerical): year of the Grammy awards
- Category (string): name of the category that the nominee won in
- Nominee (string): name of the artist, song, album, etc. that had won the grammy

 
