# Test Corrections

# Question 1 (Superhero Post) 
A friend of yours commented on a socail media post where they were asked for
their superhero name. Their superhero name is crated by combining the name
of the first street they lived on with their favorite teachers last name
and ading "-ster" to the end. Which of the following is the most appropriate
action to take?
## Answer
Advise your friend to remove their post since these are commonly used
security questions.
## My answer:
Contact the IETF(Internet Engineering Task Force)and let them know about the
issue.
## Why it's incorrect:
The IETF onlly regulates the internet protocals and has no control over
socal media. It would be best to contact the socal media site. This will
help me next time that I take a test because now I now that IETF only 
regulates internet protocals

# Question 2 (Hacking Bank Accounts)
The director of a local bank recently found out that hackers had gained
access to several usernames and passwords which link directly to individual
bank accounts. Which of the following would be the best approach to ensure
that hackers will not be able to access and of the bank accounts?
## Answer:
Enable two-factor authentication on the website so that clients must put a 
password in the website and enter a security code that is texted directly to
their phone in order to access their accounts. 
## My answer:
Update the website to ensure all server-client communications are encrypted 
using RSA encryption.
## Why it's incorrect:
Even with encryption, an unauthorized user can easily access a person's
information if they have a username with a matching password. This helps me
because now I know that even with encryption hackers can still get your 
information. But if you use two factor authentication it makes it difficult
for you to gain unsuthorized access to information

# Question 3 (App collaboration)
which of the following is the best example of using collaboration to avoid 
bias in the development of an application?
## Answer:
Sending out a survey to students from multiple schools to request feedback.
## My answer:
Coding half of an app and having a partner code the other half. 
## Why it's incorrect:
While paired programming does aid in collaboration, splitting half of the
work with a partner is not pair programming and each person may not
necessarily recive feedback from one another in this instance. This will
help me because now I know that even if you split the work with a 
partner there is still a chance of biasness and you need more input from 
other people with more diversity.

# Question 4 (Procedure Error)
John wants to create a prosedure that will return the largest even number in
a list.
PROCEDURE largestEvenNumber(listA)
{
    big = listA[1]
    index = 2
    len = LENGTH(listA)
    REPEAT UNTIL (index = len)
    {
        IF(listA[index] MOD 2 = 0   OR  listA[index] > big)
        {
                big = listA[index]
        }
    }
RETURN(big)
}
To test his method, John runs the procedure with a list of numbers. 
johnList <- 2, 5, 4, 8, 6, 13, 5
What does largestEvenNumber(joinList) return?
## Answer:
13 because in the code they use the OR operator instead of the AND operator
## My answer: 
I chose 8
## Why it's wrong:
It would have been correct if the program was writen correctly
but the program had an error so it wasn't correct. This helps me because
next time I know to look closely at the operator and see how it changes the
program.

# Question 5 (Personally Iderifiable info)
Which of the following is NOT considered personally identifiable
information?
## Answer:
Cell phone model
## What I chose: 
schools attended
## Why it's wrong:
Knowing what schools you went to could help someone find out more about
you. Many people could have baught the same cell phone model and it doesn't
help identify someone. 

# Question 6 (Binary Overflow Error)
Which of the folowing sums would result in an overflow error for binary
addition with 3 bits?
## Answer:
101+011
## My answer:
101+010
## Why it's wrong:
To have an overflow error for binary addition with 3 bits the sum of the
binary numbers in base ten must be over 7. I didn't know this before but now
that I do know that it will help me a lot. 



