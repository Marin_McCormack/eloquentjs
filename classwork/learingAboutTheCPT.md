<<<<<<< HEAD
# Question: What did I learn from reading the Create Preformance Task Student Handout from the College Board?
=======
# Question: What did I learn from reading the Create Performance Task Student Handout from the College Board?
>>>>>>> 71cfe2f63f1d1385e9a3c428ad3affb9a1073057

## Overview:
I didn't learn too many new things because we had already talked a lot about
it in class, but there were still some things that were new to me or that I 
needed a reminder about.

## Program Code:
- PDF including comments or acknowledgments for any part of the submitted
program code that has been written by someone else or someone not in your
group.

- Code must contain the following
    - Instructions for input from one of the following:
    - The user (including user actions that trigger events)
    - A device
    - An online data stream
    - A file

- Use of at least one [list](https://en.wikipedia.org/wiki/List_(abstract_data_type)) to represent a collection of data that is stored and used to
manage program complexity and help fulfill the program’s purpose.

- At least one procedure that contributes to the program’s intended purpose,
where you have defined:
    - The procedure’s name
    - The return type (if necessary)
    - One or more parameters

- An algorithm that includes sequencing, selection, and iteration that is it
the body of the selected procedure

- Calls to your student-developed procedure

- Instructions for output (tactile, audible, visual, or textual) based on input and program functionality

## Video:
- Input to your program
- At least one aspect of the functionality of your program
- Output produced by your program 

**Your video may NOT contain:**

- Any distinguishing information about yourself
- Voice narration (though text captions are encouraged)

**Your video must be:**

- Either .mp4, .wmv, .avi, or .mov format
- No more than 30MB in file size 

## Writen Responses: 
- Your response to all prompts combined must not exceed 750 words. 

