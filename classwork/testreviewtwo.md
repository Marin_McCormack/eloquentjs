# Test review

# Question 1 
## Name: Baby Genders
## Catagory: Boolean Expressions
A couple plans to have three kids. They want to estimate the probability
that they would have all three boys. Which one of the following algorithms
will **NOT** correctly compute a correct estimate of the probability?  
define 1 to designate boy and 2 to designate a girl.
## Answer choices:
### A
count ← 0

REPEAT 1000 TIMES
{
      kid1 ← RANDOM(1,2)
      kid2 ← RANDOM(1,2)
      kid3 ← RANDOM(1,2)

     IF (kid1=1 AND kid2=1  AND kid3=1)
     {
            count ← count + 1
     }
}
DISPLAY    (count/1000)
### B
count ← 0
numTrials ← 0

REPEAT UNTIL (numTrials = 1000)
{
      kid1 ← RANDOM(1,2)
      kid2 ← RANDOM(1,2)
      kid3 ← RANDOM(1,2)

     IF (kid1=2   OR  kid2=2   OR   kid3=2)
     {
          count ← count +1
     }
numTrials ← numTrials + 1
}
allBoys ← 1000 - count
DISPLAY    (count/1000)
### C
count ← 0

REPEAT 1000 TIMES
{
      kid1 ← RANDOM(1,2)
      kid2 ← RANDOM(1,2)
      kid3 ← RANDOM(1,2)

      sum ← 0

      IF (kid1=1)
       {
            sum ← sum + 1
       } 
       IF (kid2=1)
       {
            sum ← sum + 1
       }
      IF (kid3=1)
       {
            sum ← sum + 1
       }    
      IF (sum = 3) {
      {
           count ← count + 1
      }
}
DISPLAY    (count/1000)
### D
count ← 0

REPEAT 1000 TIMES
{
      kid1 ← RANDOM(1,2)
      kid2 ← RANDOM(1,2)
      kid3 ← RANDOM(1,2)

     IF (kid1=1  OR   kid2=1   OR   kid3=1)
     {
          count ← count +1
     }
}
DISPLAY    (count/1000)
## Why choice D is the correct answer: 
Choice D uses an OR boolean expression instead of an AND boolean expression
which doesn't work for what the couple is trying to figure out. The couple
wants to know the probability of having **ALL THREE BOYS** but since the
program used the OR boolean exression it would add one to count if there 
was one or more boys. Insted of the OR statement there should be an AND 
statement like in answer A. 

# Question 2 
## Name: Lossy and Lossless Text Compression Algorithms.
## Catagory: Data compression.
Examine the following two compression algorithms that are used to compress
and transmit text:
**Algorithm 1**
All vowels are removed from each word with the exception of the first letter
of the word if it is a vowel. For example, the phrase “football players are
always ready to play” would be compressed to “ftbll plyrs ar alwys rdy t
ply” and this compressed text would be transmitted.
**Algorithm 2**
Text is compressed by replacing repetitive text with a key symbol listed in
a table. For example, the phrase “they went there to see the players play
the game” could be compressed to “@y went @re to see @ #ers # @ game” using
a table that contains two key entries: “@” represents the text “the” and “#”
represents the text “play”. This table would also be transmitted along with
the compressed text.
Which of the following statements is most true about these two algorithms?
## Answer:
Algorithm 1 is an example of lossy compression and will usually reduce the
number of bits transmitted more than Algorithm 2, which is an example of
lossless compression. 
## What I chose: 
Algorithm 2 is an example of lossless compression and will usually reduce 
the number of bits transmitted more than Algorithm 1, which is an example of
lossy compression.
## Why it's incorrect: 
The lossless compression will not compress the number of bits transmitted as
much as the lossy data compression. This will help me because next time I 
see something about lossy and lossless compression I will know that lossless
compression will not compress the number of bits transmitted as much as the 
lossy data compression. I will also know that you can not get lossy data
back to the original uncompressed text but you can with lossless data.

# Question 3
## Name: Display Odds
## Catagory: The order in which the lines of code are writen.(Algorithms  &  programing)
Which of the following code segments will **NOT** correctly display the
positive, odd numbers that are less than 12?
## Answer
number ← 1

REPEAT UNTIL (number > 12)
{
    number ← number + 2
    DISPLAY(number)
} 
## What I chose: 

number ← 1

REPEAT 6 TIMES
{
        DISPLAY(number)
        number ← number + 2
}
## Why it's incorrect: 
The variable number will correctly increment by twos six times and then
stop before the number gets above 12. The correct answer is correct because
if they increment by two **before** displaying the number it wouldn't end up
displaying the number one. This will help me because now I know to play very
close attention to the order of the lines of code. A helpful tip might be to
try and see what each program would print the only problem is it could take
to much time. 

# Question 4
## Name: Interactive Arithmetic
## Catagory: Algorithms fill in the blank
The code segment below is intended to determine the quotient and remainder
of two integers when big is divided by small. Assume big is always greater
than or equal to small and both are positive integers.
**Example**

   - big = 36
   - small = 5
   - quotient should be 7 and remainder should be 1.

big  ← INPUT()
small ← INPUT()
times ← 0

REPEAT UNTIL (big < small)
{
     big ← big - small
     times ← times + 1
}

quotient ←  INSERT STATEMENT
remainder ← INSERT STATEMENT

DISPLAY(quotient)
DISPLAY(remainder)

Which of the following is the correct implementation for quotient and 
remainder?
## Answer:
quotient <- times
remainder <- big
## What I chose:
quotient <- times - 1
remainder <- small - big
## What I could have done differently
I could have taken more time on this problem and writen out each step in
the program and I think that would have helped me. Now I understand the
right answer, why it's right, and how to prove it but when I was taking the
test I didn't take my time and didnt think the algorithm through. 

# Question 5
## Name: Where Does This Packet Belong in the Message?
## Catagory: Binary numbers & packets.
Suppose a packet that is transmitted across the internet contains the
following information (from left to right):

    Bits 1-4: Packet sequence number within the message.
    Bits 5-8: Total number of packets in the message.
    Bits 9-16: Number identifying the sender.
    Bits 17-24: Number identifying the receiver.
    Bits 25-64: Part of the actual message being sent.

Here is one of the packets being sent over the internet:
01111011 10000001 11001110 01010110 00111100 10011100 11100010 10001111
Which of the following statements about this packet is true?
## Answer: 
This is packet 7 out of 11 total packets in the message.
## What I chose:
This is packet 1 out of 8 total packets in the message.
## Why it's wrong:
This would be the correct answer if bits 1-4 were 0001 (which represents decimal number 1) and bits 5-8 were 1000 (which represents decimal number 8). 
Next time I see this question I will know to write out my binary number 
system and then I will put the corresponding binary digets above the system
and calculate by adding the values with ones over them together.
 
