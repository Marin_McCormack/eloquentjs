# Marin McCormack's Study Plan

1. On which Big Ideas did you have the most difficulty on the practice test?
I had th most difficulty on Computer Systems and Networks. I got 5/7 wrong
or 72% wrong.  
I think this is because we didn't go into too much depth in class about this
Big Idea and also I don't remember that many Albert.io questions from this
Big Idea so I probobly didn't get as much practice on this Big Idea as I 
should have. 

2. Which resources do I plan to use to study the Big Ideas listed above?
- Wikipedia
- Khan Academy 
- Albet.io
- Classroom texbooks
 
3. How will I use these resources to improve your score?
   - Wikipedia will help me because if I don't know the definition of a word
I can easily find a simple definition and write it down so I have it for the
future. By the end of my studying I can have a whole set of vocabulary words
that I can turn into flashcards to use to study.
    - Khan Academy will help me improve my score because it will give me 
lessons and provide me with information while also doing periodic tests and 
quizes to see if I have retained the information it is giving me. Another 
reason this will help me is when I take the tests and quizes it will give me
back my score and I can know based on how good my score is how much more I 
might need to study that idea.
    - Albert.io will help me in a similar way that Khan Academy will help
me.Although I do think that Khan Academy will help me more, albert.io still 
could be a useful self test option. Albert.io has good quizes/ tests to 
help me understand what I might still need to work on. 
   -  Lastly I can use the resources we have in class, for example I know 
there are a lot of practice textbooks that could be useful. Using these 
practice books can help improve my score because it will get me used to the 
types of questions the real test might ask.    
 
